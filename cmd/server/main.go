package main

import (
	"context"
	"errors"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/jeremiadelviero/simple-transfer-system/internal/handler"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
)

// Client requests:
//
//	$ curl http://localhost:3333/
//	root.
//
//	$ curl http://localhost:3333/ping
//	pong
//
//	$ curl http://localhost:3333/accounts/1
//	{"status":"Invalid request.","error":"account not found"}
//
//	$ curl -X POST -d '{"account_id":123, "initial_balance":"100.23344"}' http://localhost:3333/accounts
//	{"account_id":123,"balance":"100.23344"}
//
//	$ curl http://localhost:3333/accounts/123
//	{"account_id":123,"balance":"100.23344"}
//
//	$ curl -X POST -d '{"account_id":124, "initial_balance":"100.23344"}' http://localhost:3333/accounts
//	{"account_id":124,"balance":"100.23344"}
//
//  $ curl -X POST -d '{"source_account_id":123, "destination_account_id":124, "amount":"100"}' \
//    http://localhost:3333/transactions
//  {"source_account":{"account_id":123,"balance":"0.23344"},"destination_account": \
//  {"account_id":124,"balance":"200.23344"}}

var flagUseSQL = flag.Bool("use-sql", false, "use sql")

func main() {
	flag.Parse()

	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.URLFormat)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Get("/", func(w http.ResponseWriter, _ *http.Request) {
		_, err := w.Write([]byte("root."))
		if err != nil {
			log.Fatal(err)
		}
	})

	r.Get("/ping", func(w http.ResponseWriter, _ *http.Request) {
		_, err := w.Write([]byte("pong"))
		if err != nil {
			log.Fatal(err)
		}
	})

	r.Get("/panic", func(_ http.ResponseWriter, _ *http.Request) {
		log.Fatal("test")
	})

	h := handler.NewHandler(*flagUseSQL)
	// RESTy routes for "accounts" resource
	r.Route("/accounts", func(r chi.Router) {
		r.Post("/", h.CreateAccount) // POST /accounts

		r.Route("/{accountID}", func(r chi.Router) {
			r.Use(h.AccountCtx)      // Load the *Account on the request context
			r.Get("/", h.GetAccount) // GET /accounts/123
		})
	})

	r.Post("/transactions", h.CreateTransaction) // POST /transactions

	const five = 5
	server := &http.Server{
		Addr:              ":3333",
		ReadHeaderTimeout: five * time.Second,
		Handler:           r,
	}

	// Listen for syscall signals for process to interrupt/quit
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig

		// Shutdown signal with grace period of 30 seconds
		const thirty = 30
		shutdownCtx, shutdownStopCtx := context.WithTimeout(context.Background(), thirty*time.Second)
		defer shutdownStopCtx()
		go func() {
			<-shutdownCtx.Done()
			if errors.Is(shutdownCtx.Err(), context.DeadlineExceeded) {
				log.Fatal("graceful shutdown timed out.. forcing exit.")
			}
		}()

		// Trigger graceful shutdown
		err := server.Shutdown(shutdownCtx)
		if err != nil {
			log.Fatal(err)
		}
	}()

	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
