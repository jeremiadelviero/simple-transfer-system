package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	conninfo := "user=postgres password=postgres host=localhost port=5432 sslmode=disable"
	db, err := sql.Open("postgres", conninfo)

	if err != nil {
		log.Fatal(err)
	}
	dbName := "transferdb"
	_, err = db.Exec("create database " + dbName)
	if err != nil {
		// handle the error
		log.Fatal(err)
	}
}
