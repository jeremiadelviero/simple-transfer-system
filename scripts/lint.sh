#!/usr/bin/env bash

# Source the common.sh script
# shellcheck source=./common.sh
. "$(git rev-parse --show-toplevel || echo ".")/scripts/common.sh"

if ! has ./bin/golangci-lint; then
  echo_info "Install golangci-lint for static code analysis (via curl)"
  # install into ./bin/
  # because different project might need different golang version,
  # and thus, need to use different linter version
  curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.57.2
fi

lint_fast() {
  # Run only fast linter
  ./bin/golangci-lint run --fast $*
}

lint_all() {
  # Run all the linter on all packages
  ./bin/golangci-lint run --timeout 60m $*
}

case "$1" in
all)
  shift
  lint_all $@
  exit
  ;;
*)
  lint_fast $@
  exit
  ;;
esac

EXIT_CODE=$?
exit ${EXIT_CODE}
