package handler

import "gitlab.com/jeremiadelviero/simple-transfer-system/internal/model"

type Handler struct {
	db model.Database
}

func NewHandler(usingSQL bool) *Handler {
	var db model.Database
	if usingSQL {
		db = model.NewSQLDB()
	} else {
		db = model.NewInMemoryDB()
	}
	return &Handler{db: db}
}
