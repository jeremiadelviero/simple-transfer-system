package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/jeremiadelviero/simple-transfer-system/internal/payload"

	"github.com/go-chi/render"
)

// CreateTransaction persists the posted Transaction, update balances on both accounts then
// returns the status back to the client as an acknowledgement.
func (h *Handler) CreateTransaction(w http.ResponseWriter, r *http.Request) {
	data := &payload.TransactionRequest{}
	if err := render.Bind(r, data); err != nil {
		_ = render.Render(w, r, payload.ErrInvalidRequest(fmt.Errorf("error bind: %w", err)))
		return
	}

	if err := h.db.NewTransaction(data.Transaction); err != nil {
		_ = render.Render(w, r, payload.ErrInvalidRequest(fmt.Errorf("submit transaction: %w", err)))
		return
	}

	render.Status(r, http.StatusCreated)
	_ = render.Render(w, r, payload.NewTransactionResponse(data.Transaction))
}
