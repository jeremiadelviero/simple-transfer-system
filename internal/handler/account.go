package handler

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/jeremiadelviero/simple-transfer-system/internal/model"
	"gitlab.com/jeremiadelviero/simple-transfer-system/internal/payload"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type accountCtxKeyType int

const accountCtxKey = accountCtxKeyType(1)

// AccountCtx middleware is used to load an Account object from
// the URL parameters passed through as the request. In case
// the Account could not be found, we stop here and return a 404.
func (h *Handler) AccountCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accountID, err := strconv.ParseInt(chi.URLParam(r, "accountID"), 10, 64)
		if err != nil {
			_ = render.Render(w, r, payload.ErrInvalidRequest(errors.New("account_id is invalid")))
			return
		}
		account, err := h.db.GetAccount(accountID)
		if err != nil {
			_ = render.Render(w, r, payload.ErrInvalidRequest(err))
			return
		}

		ctx := context.WithValue(r.Context(), accountCtxKey, account)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// CreateAccount persists the posted Account and returns it
// back to the client as an acknowledgement.
func (h *Handler) CreateAccount(w http.ResponseWriter, r *http.Request) {
	data := &payload.CreateAccountRequest{}
	if err := render.Bind(r, data); err != nil {
		_ = render.Render(w, r, payload.ErrInvalidRequest(fmt.Errorf("error bind %w", err)))
		return
	}

	if err := h.db.NewAccount(data.Account); err != nil {
		_ = render.Render(w, r, payload.ErrInvalidRequest(fmt.Errorf("failed to create new account: %w", err)))
		return
	}

	render.Status(r, http.StatusCreated)
	_ = render.Render(w, r, payload.NewAccountResponse(data.Account))
}

// GetAccount returns the specific model.Account. You'll notice it just
// fetches the model.Account right off the context, as its understood that
// if we made it this far, the model.Account must be on the context. In case
// its not due to a bug, then it will panic, and our middleware.Recoverer will save us.
func (h *Handler) GetAccount(w http.ResponseWriter, r *http.Request) {
	account, ok := r.Context().Value(accountCtxKey).(*model.Account)
	if !ok {
		_ = render.Render(w, r, payload.ErrInvalidRequest(errors.New("account not found")))
	}

	if err := render.Render(w, r, payload.NewAccountResponse(account)); err != nil {
		_ = render.Render(w, r, payload.ErrRender(err))
		return
	}
}
