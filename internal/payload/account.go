package payload

import (
	"errors"
	"net/http"

	"gitlab.com/jeremiadelviero/simple-transfer-system/internal/model"
)

// CreateAccountRequest is the request payload for *model.Account data model.
//
// NOTE: It's good practice to have well defined request and response payloads
// so you can manage the specific inputs and outputs for clients, and also gives
// you the opportunity to transform data on input or output, for example
// on request, we'd like to protect certain fields and on output perhaps
// we'd like to include a computed field based on other values that aren't
// in the data model. Also, check out this awesome blog post on struct composition:
// http://attilaolah.eu/2014/09/10/json-and-struct-composition-in-go/
type CreateAccountRequest struct {
	*model.Account
	InitialBalance string `json:"initial_balance"`
}

func (a *CreateAccountRequest) Bind(_ *http.Request) error {
	// a.Account is nil if no Account fields are sent in the request. Return an
	// error to avoid a nil pointer dereference.
	if a.Account == nil {
		return errors.New("missing required Account fields")
	}
	a.Account.Balance = a.InitialBalance // set the balance
	return nil
}

// AccountResponse is the response payload for the Account data model.
// See NOTE above in CreateAccountRequest as well.
//
// In the AccountResponse object, first a Render() is called on itself,
// then the next field, and so on, all the way down the tree.
// Render is called in top-down order, like a http handler middleware chain.
type AccountResponse struct {
	*model.Account
}

func NewAccountResponse(account *model.Account) *AccountResponse {
	return &AccountResponse{
		Account: account,
	}
}

func (rd *AccountResponse) Render(_ http.ResponseWriter, _ *http.Request) error {
	return nil
}
