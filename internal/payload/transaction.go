package payload

import (
	"errors"
	"net/http"

	"gitlab.com/jeremiadelviero/simple-transfer-system/internal/model"
)

// TransactionRequest is the request payload for *model.Transaction data model.
type TransactionRequest struct {
	*model.Transaction
}

func (a *TransactionRequest) Bind(_ *http.Request) error {
	// a.Transaction is nil if no Transaction fields are sent in the request. Return an
	// error to avoid a nil pointer dereference.
	if a.Transaction == nil {
		return errors.New("missing required Transaction fields")
	}
	return nil
}

// TransactionResponse is the response payload for the Transaction data model.
// See NOTE above in CreateAccountRequest as well.
//
// In the TransactionResponse object, first a Render() is called on itself,
// then the next field, and so on, all the way down the tree.
// Render is called in top-down order, like a http handler middleware chain.
type TransactionResponse struct {
	*model.Transaction
}

func NewTransactionResponse(transaction *model.Transaction) *TransactionResponse {
	return &TransactionResponse{
		Transaction: transaction,
	}
}

func (rd *TransactionResponse) Render(_ http.ResponseWriter, _ *http.Request) error {
	return nil
}
