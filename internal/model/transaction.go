package model

import (
	"gorm.io/gorm"
)

type Transaction struct {
	gorm.Model           `json:"-"`
	SourceAccountID      int64  `json:"source_account_id" gorm:"not null"`
	DestinationAccountID int64  `json:"destination_account_id" gorm:"not null"`
	Amount               string `json:"amount" gorm:"not null"`
}
