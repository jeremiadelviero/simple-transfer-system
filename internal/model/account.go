package model

import (
	"fmt"
	"math/big"
)

const currencyPrecision = 5

type Account struct {
	AccountID int64  `json:"account_id" gorm:"primaryKey"`
	Balance   string `json:"balance" gorm:"not null"`
}

func (a *Account) TransferBalance(dest *Account, amount string) error {
	sourceAccBalance, ok := (&big.Rat{}).SetString(a.Balance)
	if !ok {
		return fmt.Errorf("parsing source account balance %s", a.Balance)
	}
	destAccBalance, ok := (&big.Rat{}).SetString(dest.Balance)
	if !ok {
		return fmt.Errorf("parsing destination account balance %s", dest.Balance)
	}
	transferAmount, ok := (&big.Rat{}).SetString(amount)
	if !ok {
		return fmt.Errorf("parsing amount %s", amount)
	}

	if transferAmount.Cmp(&big.Rat{}) <= 0 {
		return fmt.Errorf("cannot transfer with invalid amount %s", amount)
	}

	if sourceAccBalance.Cmp(transferAmount) < 0 {
		return fmt.Errorf("account balance is insufficient (less than %s)", amount)
	}
	sourceAccBalance = sourceAccBalance.Sub(sourceAccBalance, transferAmount)
	destAccBalance = destAccBalance.Add(destAccBalance, transferAmount)

	a.Balance = sourceAccBalance.FloatString(currencyPrecision)
	dest.Balance = destAccBalance.FloatString(currencyPrecision)
	return nil
}
