package model

import (
	"errors"
	"fmt"
	"sync"
)

type InMemory struct {
	sync.RWMutex
	accountMap   map[int64]*Account
	transactions []*Transaction
}

func NewInMemoryDB() *InMemory {
	return &InMemory{accountMap: make(map[int64]*Account)}
}

func (db *InMemory) NewAccount(account *Account) error {
	db.Lock()
	defer db.Unlock()
	if db.accountMap[account.AccountID] != nil {
		return errors.New("account already exists")
	}
	db.accountMap[account.AccountID] = account
	return nil
}

// private method for getAccount without lock.
func (db *InMemory) getAccount(id int64) (*Account, error) {
	if val := db.accountMap[id]; val != nil {
		return val, nil
	}
	return nil, errors.New("account not found")
}

func (db *InMemory) GetAccount(id int64) (*Account, error) {
	db.RLock()
	defer db.RUnlock()
	return db.getAccount(id)
}

func (db *InMemory) NewTransaction(transaction *Transaction) error {
	db.Lock()
	defer db.Unlock()
	sourceAccount, err := db.getAccount(transaction.SourceAccountID)
	if err != nil {
		return fmt.Errorf("error getting source account_id %d: %w", transaction.SourceAccountID, err)
	}

	destinationAccount, err := db.getAccount(transaction.DestinationAccountID)
	if err != nil {
		return fmt.Errorf("error getting destination account_id %d: %w", transaction.DestinationAccountID, err)
	}

	err = sourceAccount.TransferBalance(destinationAccount, transaction.Amount)
	if err != nil {
		return err
	}

	db.transactions = append(db.transactions, transaction)
	return nil
}
