package model

type Database interface {
	NewAccount(account *Account) error
	GetAccount(id int64) (*Account, error)
	NewTransaction(transaction *Transaction) error
}
