package model

import (
	"errors"
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type SQL struct {
	*gorm.DB
}

func NewSQLDB() *SQL {
	dsn := "user=postgres password=postgres host=localhost port=5432 dbname=transferdb sslmode=disable"
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	err = database.AutoMigrate(&Account{}, &Transaction{})
	if err != nil {
		panic(err)
	}
	return &SQL{DB: database}
}

func (db *SQL) NewAccount(account *Account) error {
	result := db.Create(account)
	return result.Error
}

// private method for getAccount.
func (db *SQL) getAccount(id int64) (*Account, error) {
	var account *Account
	result := db.First(&account, id)
	if result.Error != nil {
		return nil, errors.New("account not found")
	}
	return account, nil
}

func (db *SQL) GetAccount(id int64) (*Account, error) {
	return db.getAccount(id)
}

func (db *SQL) NewTransaction(transaction *Transaction) error {
	sourceAccount, err := db.getAccount(transaction.SourceAccountID)
	if err != nil {
		return fmt.Errorf("error getting source account_id %d: %w", transaction.SourceAccountID, err)
	}

	destinationAccount, err := db.getAccount(transaction.DestinationAccountID)
	if err != nil {
		return fmt.Errorf("error getting destination account_id %d: %w", transaction.DestinationAccountID, err)
	}

	err = sourceAccount.TransferBalance(destinationAccount, transaction.Amount)
	if err != nil {
		return err
	}

	result := db.Save(sourceAccount)
	if result.Error != nil {
		return result.Error
	}
	result = db.Save(destinationAccount)
	if result.Error != nil {
		return result.Error
	}
	return db.Create(transaction).Error
}
