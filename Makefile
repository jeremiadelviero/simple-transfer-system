all: server

server: cmd/server/main.go
	go build -o bin/$@ $^

createdb: cmd/createdatabase/main.go
	go build -o bin/$@ $^

test: ## Generate mock and run all test. To run specified tests, use `./scripts/test.sh <pattern>`)
	@scripts/test.sh $*

.PHONY: lint
lint: ## Run linter with --fast option, for most common issues
	@scripts/lint.sh

.PHONY: lint/all
lint/all: ## Run all the linter, slow, but stricter
	@scripts/lint.sh all