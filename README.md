# Simple Transfer System

## Prerequisites
* **Golang v1.22.2+**
* **PostgreSQL 16+**
* **GNU Make** installed

## Installation and Run
* For **In-memory DB** server run these commands:
```
make server
./bin/server
```

* For **PostgreSQL DB** server:
1. Start PostgreSQL with these credentials:
```
user=postgres
password=postgres
host=localhost
port=5432
```
2. Run these commands
```
make createdb
make server
./bin/server -use-sql
```

## Assumptions
* Currency is the same for all accounts
* System security is not necessary for now
* Currency format is string with **5 decimal digits** / **5 digits to the right of the decimal point**
* Invalid account_id / balance / transfer amount already handled
* Concurrency read and write is handled using in-memory database

## Request Examples
```
$ curl http://localhost:3333/
root.

$ curl http://localhost:3333/ping
pong

$ curl http://localhost:3333/accounts/1
{"status":"Invalid request.","error":"account not found"}

$ curl -X POST -d '{"account_id":123, "initial_balance":"100.23344"}' http://localhost:3333/accounts
{"account_id":123,"balance":"100.23344"}

$ curl http://localhost:3333/accounts/123
{"account_id":123,"balance":"100.23344"}

$ curl -X POST -d '{"account_id":124, "initial_balance":"100.23344"}' http://localhost:3333/accounts
{"account_id":124,"balance":"100.23344"}

$ curl -X POST -d '{"source_account_id":123, "destination_account_id":124, "amount":"100"}' http://localhost:3333/transactions
{"source_account_id":123, "destination_account_id":124, "amount":"100"}

$ curl http://localhost:3333/accounts/123
{"account_id":123,"balance":"0.23344"}

$ curl http://localhost:3333/accounts/124
{"account_id":124,"balance":"200.23344"}
```
